let FAClient = null;
let notyf = null;
let notificationOpen = false;

const SERVICE = {
    name: 'FreeAgentService',
    appletId: 'aHR0cHM6Ly9mYS1zb2x1dGlvbnMuZ2l0bGFiLmlvL2ZhLXByb21wdA==' //aHR0cDovL2xvY2FsaG9zdDo4MDAw,
};


function startupService() {
    notyf = new Notyf({
        duration: 20000,
        dismissible: true,
        position: {
            x: 'center',
            y: 'bottom',
        },
        types: [
            {
                type: 'info',
                className: 'info-notyf',
                icon: false,
            },
        ],
    });

    FAClient = new FAAppletClient({
        appletId: SERVICE.appletId,
    });

    FAClient.on('openPrompt', ({record, appConfiguration}) => {
        console.log(record, appConfiguration);
        let container = document.querySelector('#frameContainer');

        container.innerHTML = `<div class="card">
            <h6 class="card-header">Do you want to create a new task for ${record.field_values.fa_entity_reference_id.display_value} ?</h6>
            <div class="card-body">
              <div id="addTask" class="btn btn-primary">Next Step</div>
              <div id="addMeeting" class="btn btn-primary">Meeting</div>
              <div id="addEmail" class="btn btn-primary">Email</div>
              <div id="addPhone" class="btn btn-primary">Phone</div>
              <div id="cancel" class="btn btn-secondary">Cancel</a>
            </div>
          </div>`


        let addNewTodo = container.querySelector('#addTask');
        let addNewMeeting = container.querySelector('#addMeeting');
        let addNewEmail = container.querySelector('#addEmail');
        let addNewPhone = container.querySelector('#addPhone');
        let cancel = container.querySelector('#cancel');

        let addDescription = () => container.innerHTML = `<div class="description">Please click "Complete Task" app action to continue.</div>`;

        addNewTodo.addEventListener('click', (e) => {
            createTask(record, appConfiguration, 'todo');
            addDescription()
        })

        addNewMeeting.addEventListener('click', (e) => {
            createTask(record, appConfiguration, 'meeting');
            addDescription()
        });

        addNewEmail.addEventListener('click', (e) => {
            createTask(record, appConfiguration, 'email');
            addDescription()
        });

        addNewPhone.addEventListener('click', (e) => {
            createTask(record, appConfiguration, 'phone');
            addDescription()
        });

        cancel.addEventListener('click', (e) => {
            addDescription()
        });

        FAClient.open();
    });

    FAClient.on('openForm', ({url}) => {
        document.getElementById('frameContainer').innerHTML = '';
        const iFrame = document.createElement('iframe');
        iFrame.src = url;
        iFrame.style = 'width: 100vw; min-width: 100vw;min-height: 100vh; height: 100vh; border: none;';
        document.getElementById('frameContainer').appendChild(iFrame);
        FAClient.open();
    });
}


let createTask = (record, appConfiguration, type) => {
    let taskTypeId = {
        email: "60242010-69b1-4cf6-96cd-8779f5950256", //"e7af876c-8f9d-40ef-8e63-ffeced17abaa",
        todo: "31553dd3-d9ad-4f5d-bf94-ff129f95001b", // "7ef2155f-032a-44e1-ab5e-88d181709c8a",
        meeting: "0c3c1eae-4e95-42c4-bdd4-4797f94bd8f5", //"a8e084b9-d0f0-4c47-9d0f-b565e9ea544a",
        phone: "e66b5633-87ad-454a-bb92-d9a5670710cb", // "c2165a88-ce01-48b0-b48b-b51445595fa7"
    }
    const entity = appConfiguration;
    FAClient.createEntity({
        entity: entity.name,
        field_values: {
            task_status: false,
            task_type: taskTypeId[type],
            assigned_to: record.field_values.assigned_to.value,
            fa_entity_id: record.field_values.fa_entity_id.value,
            fa_entity_reference_id: record.field_values.fa_entity_reference_id.value
        },
    }, (task) => {
        console.log(task);
        let entityInstance = {
            ...task.entity_value
        };
        FAClient.showModal('entityFormModal', {
            entity: entity.name,
            entityLabel: entity.label,
            entityInstance: entityInstance,
            showButtons: false,
        });
    });
    return;
}








